﻿using BibliotecaUPN.Web.Controllers;
using BibliotecaUPN.Web.Interfaces;
using BibliotecaUPN.Web.Manager;
using BibliotecaUPN.Web.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PruebasBibliotecaUPN.Unitarias
{
    [TestFixture]
    class BibliotecaControllerTest
    {
        [Test]
        public void TestBibliotecaIndex()
        {
            var serviceBibliotecaMock = new Mock<IBibliotecaService>();

            var controlador = new BibliotecaController(serviceBibliotecaMock.Object);
            var vista = controlador.Index();
            Assert.IsInstanceOf<ViewResult>(vista);
        }

        [Test]
        public void TestBibliotecaAdd()
        {
            var serviceBibliotecaMock = new Mock<IBibliotecaService>();

            serviceBibliotecaMock.Setup(o => o.ExisteLibroEnLaBiblioteca(1)).Returns(false);
            var controlador = new BibliotecaController(serviceBibliotecaMock.Object);
            var vista = controlador.Add(3);
            Assert.IsInstanceOf<RedirectToRouteResult>(vista);
        }

        [Test]
        public void TestBibliotecaAddExiste()
        {
            var serviceBibliotecaMock = new Mock<IBibliotecaService>();

            serviceBibliotecaMock.Setup(o => o.ExisteLibroEnLaBiblioteca(1)).Returns(true);

            var controlador = new BibliotecaController(serviceBibliotecaMock.Object);
            var vista = controlador.Add(1);
            Assert.IsInstanceOf<RedirectToRouteResult>(vista);
        }

        [Test]
        public void TestBibliotecaMarcarComoLeyendo()
        {
            var serviceBibliotecaMock = new Mock<IBibliotecaService>();

            var controlador = new BibliotecaController(serviceBibliotecaMock.Object);
            var vista = controlador.MarcarComoLeyendo(1);
            Assert.IsInstanceOf<RedirectToRouteResult>(vista);
        }

        [Test]
        public void TestBibliotecaMarcarComoTerminado()
        {
            var serviceBibliotecaMock = new Mock<IBibliotecaService>();

            var controlador = new BibliotecaController(serviceBibliotecaMock.Object);
            var vista = controlador.MarcarComoTerminado(1);
            Assert.IsInstanceOf<RedirectToRouteResult>(vista);
        }
    }
}
