﻿using BibliotecaUPN.Web.Controllers;
using BibliotecaUPN.Web.Interfaces;
using BibliotecaUPN.Web.Manager;
using BibliotecaUPN.Web.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PruebasBibliotecaUPN.Unitarias
{
    [TestFixture]
    class AuthControllerTest
    {
        [Test]
        public void TestAuthLogin()
        {
            var serviceSessionMock = new Mock<ISessionService>();
            var serviceAutManagerMock = new Mock<IAutManager>();
            var serviceAuthManagerMock = new Mock<IAuthService>();

            var controlador = new AuthController(serviceSessionMock.Object, serviceAutManagerMock.Object, serviceAuthManagerMock.Object);
            var vista = controlador.Login();
            Assert.IsInstanceOf<ViewResult>(vista);
        }

        [Test]
        public void TestAuthLoginForm()
        {
            var serviceSessionMock = new Mock<ISessionService>();
            var serviceAutManagerMock = new Mock<IAutManager>();
            var serviceAuthManagerMock = new Mock<IAuthService>();

            serviceAuthManagerMock.Setup(u => u.GetUsuarioByUsernameAndClave("admin", "admin")).Returns(new Usuario() { Id = 1 });

            var controlador = new AuthController(serviceSessionMock.Object, serviceAutManagerMock.Object, serviceAuthManagerMock.Object);
            var vista = controlador.Login("admin","admin");
            Assert.IsInstanceOf<RedirectToRouteResult>(vista);
        }

        [Test]
        public void TestAuthLoginFormEmpty()
        {
            var serviceSessionMock = new Mock<ISessionService>();
            var serviceAutManagerMock = new Mock<IAutManager>();
            var serviceAuthManagerMock = new Mock<IAuthService>();

            var controlador = new AuthController(serviceSessionMock.Object, serviceAutManagerMock.Object, serviceAuthManagerMock.Object);
            var vista = controlador.Login("", "");
            Assert.IsInstanceOf<ViewResult>(vista);
        }
    }
}
