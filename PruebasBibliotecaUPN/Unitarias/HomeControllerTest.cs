﻿using BibliotecaUPN.Web.Controllers;
using BibliotecaUPN.Web.Interfaces;
using BibliotecaUPN.Web.Manager;
using BibliotecaUPN.Web.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PruebasBibliotecaUPN.Unitarias
{
    [TestFixture]
    class HomeControllerTest
    {
        [Test]
        public void TestHomeIndex()
        {
            var serviceLibroMock = new Mock<ILibroService>();

            var controlador = new HomeController(serviceLibroMock.Object);
            var vista = controlador.Index();
            Assert.IsInstanceOf<ViewResult>(vista);
        }
    }
}
