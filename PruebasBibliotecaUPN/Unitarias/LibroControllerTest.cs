﻿using BibliotecaUPN.Web.Controllers;
using BibliotecaUPN.Web.Interfaces;
using BibliotecaUPN.Web.Manager;
using BibliotecaUPN.Web.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PruebasBibliotecaUPN.Unitarias
{
    [TestFixture]
    class LibroControllerTest
    {
        [Test]
        public void TestLibroDetail()
        {
            var serviceLibroMock = new Mock<ILibroService>();
            var serviceSessionMock = new Mock<ISessionService>();
            var serviceComentarioMock = new Mock<IComentarioService>();

            serviceLibroMock.Setup(o => o.GetLibroById(1)).Returns(new Libro());

            var controlador = new LibroController(serviceLibroMock.Object, serviceSessionMock.Object, serviceComentarioMock.Object);
            var vista = controlador.Details(1);
            Assert.IsInstanceOf<ViewResult>(vista);
        }

        [Test]
        public void TestLibroAddComentario()
        {
            var serviceLibroMock = new Mock<ILibroService>();
            var serviceSessionMock = new Mock<ISessionService>();
            var serviceComentarioMock = new Mock<IComentarioService>();

            serviceLibroMock.Setup(o => o.LibroComentarTermino(new Comentario
            {
                Id = 1,
                Fecha = DateTime.Now,
                Puntaje = 3,
                Texto = "mock",
                LibroId = 1,
                UsuarioId = 1
            })).Returns(true);

            serviceLibroMock.Setup(o => o.GetLibroById(1)).Returns(new Libro());

            var controlador = new LibroController(serviceLibroMock.Object, serviceSessionMock.Object, serviceComentarioMock.Object);
            var vista = controlador.AddComentario(new Comentario {
                Id = 1,
                Fecha = DateTime.Now,
                Puntaje = 3,
                Texto = "mock",
                LibroId = 1,
                UsuarioId = 1
            });
            Assert.IsInstanceOf<RedirectToRouteResult>(vista);
        }

        [Test]
        public void TestLibroAddComentarioNoFinalizadoElLibro()
        {
            var serviceLibroMock = new Mock<ILibroService>();
            var serviceSessionMock = new Mock<ISessionService>();
            var serviceComentarioMock = new Mock<IComentarioService>();

            serviceLibroMock.Setup(o => o.LibroComentarTermino(new Comentario
            {
                Id = 1,
                Fecha = DateTime.Now,
                Puntaje = 3,
                Texto = "mock",
                LibroId = 1,
                UsuarioId = 1
            })).Returns(false);

            serviceLibroMock.Setup(o => o.GetLibroById(1)).Returns(new Libro());

            var controlador = new LibroController(serviceLibroMock.Object, serviceSessionMock.Object, serviceComentarioMock.Object);
            var vista = controlador.AddComentario(new Comentario
            {
                Id = 1,
                Fecha = DateTime.Now,
                Puntaje = 3,
                Texto = "mock",
                LibroId = 1,
                UsuarioId = 1
            });
            Assert.IsInstanceOf<RedirectToRouteResult>(vista);
        }
    }
}
