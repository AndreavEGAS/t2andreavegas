﻿using BibliotecaUPN.Web.DB;
using BibliotecaUPN.Web.Interfaces;
using BibliotecaUPN.Web.Manager;
using BibliotecaUPN.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace BibliotecaUPN.Web.Controllers
{
    public class AuthController : Controller
    {
        private readonly ISessionService session;
        private readonly IAutManager autenticacion;
        private readonly IAuthService AuthService;

        public AuthController(ISessionService session, IAutManager autenticacion, IAuthService AuthService)
        {
            this.session = session;
            this.autenticacion = autenticacion;
            this.AuthService = AuthService;
        }

        [HttpGet]
        public ActionResult Login()
        {            
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            var usuario = AuthService.GetUsuarioByUsernameAndClave(username, password);
            if (usuario != null)
            {
                autenticacion.Login(usuario);
                session.GuardarSesion(usuario);
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Validation = "Usuario y/o contraseña incorrecta";
            return View();
        }


        public ActionResult Logout() {
            autenticacion.Logout();
            session.LimpiarSesion();
            return RedirectToAction("Login");
        }
    }
}