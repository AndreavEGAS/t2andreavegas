﻿using BibliotecaUPN.Web.DB;
using BibliotecaUPN.Web.Interfaces;
using BibliotecaUPN.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BibliotecaUPN.Web.Controllers
{
    public class LibroController : Controller
    {
        private readonly ILibroService servicioLibro;
        private readonly ISessionService session;
        private readonly IComentarioService servicioComentario;

        public LibroController(ILibroService servicioLibro, ISessionService session, IComentarioService servicioComentario)
        {
            this.servicioLibro = servicioLibro;
            this.session = session;
            this.servicioComentario = servicioComentario;
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            return View(servicioLibro.GetLibroById(id));
        }

        [HttpPost]
        public ActionResult AddComentario(Comentario comentario)
        {
            if (servicioLibro.LibroComentarTermino(comentario))
            {
                servicioComentario.GuadarComentario(comentario);
                servicioLibro.AgregarPuntajeLibro(comentario);
                return RedirectToAction("Details", new { id = comentario.LibroId });
            }
            TempData["ErrorMessage"] = "No Puedes Comentar hasta terminar el libro";
            return RedirectToAction("Details", new { id = comentario.LibroId });

        }
    }
}