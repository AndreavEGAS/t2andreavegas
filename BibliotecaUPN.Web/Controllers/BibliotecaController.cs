﻿using BibliotecaUPN.Web.Constantes;
using BibliotecaUPN.Web.DB;
using BibliotecaUPN.Web.Interfaces;
using BibliotecaUPN.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BibliotecaUPN.Web.Controllers
{
    [Authorize]
    public class BibliotecaController : Controller
    {
        private readonly IBibliotecaService servicioBiblioteca;

        public BibliotecaController(IBibliotecaService servicioBiblioteca)
        {
            this.servicioBiblioteca = servicioBiblioteca;
        }

        [HttpGet]
        public ActionResult Index()
        {   
            return View(servicioBiblioteca.GetBibliotecaAsListByUserId());
        }

        [HttpGet]
        public ActionResult Add(int libro)
        {
            if (servicioBiblioteca.ExisteLibroEnLaBiblioteca(libro))
            {
                TempData["ErrorMessage"] = "El Libro ya esta en tu biblioteca";
                return RedirectToAction("Index", "Home");
            }

            servicioBiblioteca.GuardarEnLaBiblioteca(libro);
            TempData["SuccessMessage"] = "Se añádio el libro a su biblioteca";
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult MarcarComoLeyendo(int libroId)
        {
            servicioBiblioteca.MarcarLibroComoLeido(libroId);         
            TempData["SuccessMessage"] = "Se marco como leyendo el libro";
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult MarcarComoTerminado(int libroId)
        {
            servicioBiblioteca.MarcarLibroComoTerminado(libroId);
            TempData["SuccessMessage"] = "Se marco como leyendo el libro";
            return RedirectToAction("Index");
        }
    }
}