﻿using BibliotecaUPN.Web.DB;
using BibliotecaUPN.Web.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BibliotecaUPN.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILibroService servicioLibro;
        public HomeController(ILibroService servicioLibro)
        {
            this.servicioLibro = servicioLibro;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View(servicioLibro.GetLibrosAsList());
        }       

    }
}