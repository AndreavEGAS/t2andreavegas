﻿using BibliotecaUPN.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotecaUPN.Web.Interfaces
{
    public interface IBibliotecaService
    {
        Biblioteca GetLibroByIdAndUserId(int LibroId);
        List<Biblioteca> GetBibliotecaAsListByUserId();
        void GuardarEnLaBiblioteca(int Libro);
        void MarcarLibroComoLeido(int LibroId);
        void MarcarLibroComoTerminado(int LibroId);
        bool ExisteLibroEnLaBiblioteca(int LibroId);
    }
}
