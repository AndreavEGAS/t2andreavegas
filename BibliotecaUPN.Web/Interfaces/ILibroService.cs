﻿using BibliotecaUPN.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BibliotecaUPN.Web.Interfaces
{
    public interface ILibroService
    {
        List<Libro> GetLibrosAsList();
        Libro GetLibroById(int LibroId);
        void AgregarPuntajeLibro(Comentario comentario);
        bool LibroComentarTermino(Comentario comentario);
    }
}
