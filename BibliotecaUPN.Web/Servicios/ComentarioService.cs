﻿using BibliotecaUPN.Web.DB;
using BibliotecaUPN.Web.Interfaces;
using BibliotecaUPN.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BibliotecaUPN.Web.Servicios
{
    public class ComentarioService : IComentarioService
    {
        private readonly AppContext conexion;
        private readonly SessionService session;

        public ComentarioService(AppContext conexion)
        {
            this.conexion = conexion;
            session = new SessionService();
        }

        public void GuadarComentario (Comentario comentario)
        {
            comentario.UsuarioId = session.ConvertirSessionIdAIntId();
            comentario.Fecha = DateTime.Now;
            conexion.Comentarios.Add(comentario);
            conexion.SaveChanges();
        }
    }
}