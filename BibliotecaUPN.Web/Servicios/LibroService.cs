﻿using BibliotecaUPN.Web.Constantes;
using BibliotecaUPN.Web.DB;
using BibliotecaUPN.Web.Interfaces;
using BibliotecaUPN.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BibliotecaUPN.Web.Servicios
{
    public class LibroService : ILibroService
    {
        private readonly AppContext conexion;
        private readonly BibliotecaService servicioBiblioteca;

        public LibroService(AppContext conexion)
        {
            this.conexion = conexion;
            servicioBiblioteca = new BibliotecaService(conexion);
        }

        public List<Libro> GetLibrosAsList()
        {
            return conexion.Libros.Include(o => o.Autor).ToList();
        }

        public Libro GetLibroById(int LibroId)
        {
            Libro libro = conexion.Libros.Include(o => o.Autor)
                .Include(o => o.Comentarios.Select(x => x.Usuario))
                .Where(o => o.Id == LibroId)
                .FirstOrDefault();

            return libro;
        }

        public Libro GetLibroByComentarioId(int LibroId)
        {
            Libro libro = conexion.Libros.Where(o => o.Id == LibroId).FirstOrDefault();
            return libro;
        }

        public void AgregarPuntajeLibro(Comentario comentario)
        {
            Libro libro = GetLibroByComentarioId(comentario.LibroId);

            libro.Puntaje = (libro.Puntaje + comentario.Puntaje) / 2;
            conexion.SaveChanges();
        }

        public bool LibroComentarTermino(Comentario comentario)
        {
            var Biblioteca = servicioBiblioteca.GetBibliotecaAsListByUserId();
            Libro libro = GetLibroByComentarioId(comentario.LibroId);

            foreach (var biblioteca in Biblioteca)
            {
                if (biblioteca.LibroId == libro.Id)
                    if(biblioteca.Estado == ESTADO.TERMINADO)
                        return true;
                    return false;                   
            }
            return false;
        }
    }
}