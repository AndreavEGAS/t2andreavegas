﻿using BibliotecaUPN.Web.Constantes;
using BibliotecaUPN.Web.DB;
using BibliotecaUPN.Web.Interfaces;
using BibliotecaUPN.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BibliotecaUPN.Web.Servicios
{
    public class BibliotecaService : IBibliotecaService
    {
        private readonly AppContext conexion;
        private readonly SessionService session;

        public BibliotecaService(AppContext conexion)
        {
            this.conexion = conexion;
            session = new SessionService();
        }

        public Biblioteca GetLibroByIdAndUserId(int LibroId)
        {
            int UsuarioId = session.ConvertirSessionIdAIntId();
            Biblioteca libro = conexion.Bibliotecas
                .Where(o => o.LibroId == LibroId && o.UsuarioId == UsuarioId)
                .FirstOrDefault();

            return libro;
        }

        public List<Biblioteca> GetBibliotecaAsListByUserId()
        {
            int UsuarioId = session.ConvertirSessionIdAIntId();

            return conexion.Bibliotecas
                .Include(o => o.Libro.Autor)
                .Include(o => o.Usuario)
                .Where(o => o.UsuarioId == UsuarioId)
                .ToList();
        }

        public void GuardarEnLaBiblioteca(int Libro)
        {
            int UsuarioId = session.ConvertirSessionIdAIntId();

            Biblioteca biblioteca = new Biblioteca
            {
                LibroId = Libro,
                UsuarioId = UsuarioId,
                Estado = ESTADO.POR_LEER
            };

            conexion.Bibliotecas.Add(biblioteca);
            conexion.SaveChanges();
        }

        public void MarcarLibroComoLeido(int LibroId)
        {
            Biblioteca libro = GetLibroByIdAndUserId(LibroId);
            libro.Estado = ESTADO.LEYENDO;
            conexion.SaveChanges();
        }

        public void MarcarLibroComoTerminado(int LibroId)
        {
            Biblioteca libro = GetLibroByIdAndUserId(LibroId);
            libro.Estado = ESTADO.TERMINADO;
            conexion.SaveChanges();
        }

        public bool ExisteLibroEnLaBiblioteca(int LibroId)
        {
            var Biblioteca = GetBibliotecaAsListByUserId();

            foreach (var libros in Biblioteca)
            {
                if (libros.LibroId == LibroId)
                    return true;
            }
            return false;
        }

    }
}