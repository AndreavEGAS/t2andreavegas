﻿using BibliotecaUPN.Web.DB;
using BibliotecaUPN.Web.Interfaces;
using BibliotecaUPN.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BibliotecaUPN.Web.Servicios
{
    public class AuthService : IAuthService
    {
        private readonly AppContext conexion;

        public AuthService(AppContext conexion)
        {
            this.conexion = conexion;
        }

        public Usuario GetUsuarioByUsernameAndClave(string username, string password)
        {
            Usuario usuario = conexion.Usuarios.Where(o => o.Username == username && o.Password == password).FirstOrDefault();

            if (usuario == null)
                return null;

            return usuario;
        }
    }
}